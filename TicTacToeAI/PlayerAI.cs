﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToeAI
{
    public class PlayerAI
    {
        public string Pawn { get; }

        public readonly static Random rnd = new Random();

        public Dictionary<string, double> QualityBoard { get; } = new Dictionary<string, double>();

        public PlayerAI(string pawn)
        {
            Pawn = pawn;
        }

        public int GetRandomNextCase(Board board)
        {
            List<int> possibleStates = board.GetPossibleCase().ToList();
            return possibleStates[rnd.Next(0, possibleStates.Count)];
        }

        public void AIPlay(Board board, PlayerAI otherPlayer, string nextPawn, double lrnRate, double gamma)
        {
            var possibilities = board.GetPossibleCase().ToList();
            double valQ = double.MinValue;
            for (int j = 0; j < possibilities.Count(); j++)
            {
                var key = board.ToHash(possibilities[j], nextPawn);

                double q = QualityBoard.TryGetValue(key, out var value) ? value : 0;
                if (q >= valQ)
                {
                    valQ = q;
                }
            }

            RegisterQuality(board, otherPlayer, lrnRate, gamma, valQ);
        }

        public void RegisterQuality(Board board, PlayerAI otherPlayer, double lrnRate, double gamma, double valQ)
        {
            var currentQuality = QualityBoard.ContainsKey(board.ToHash()) ? QualityBoard[board.ToHash()] : 0;
            var quality = ((1 - lrnRate) * currentQuality) + (lrnRate * (GetReward(board, otherPlayer) + (gamma * valQ)));
            if (QualityBoard.ContainsKey(board.ToHash()))
            {
                QualityBoard[board.ToHash()] = quality;
            }
            else
            {
                QualityBoard.Add(board.ToHash(), quality);
            }
        }

        public double GetReward(Board board, PlayerAI otherPlayer)
        {
            if (board.TryGetWinner(Pawn))
                return 10;

            //if (board.TryGetWinner(otherPlayer.Pawn))
             //   return -10;

            return -(1/ board.GetPawnNumber(otherPlayer.Pawn));
        }

        public string ToQualityString()
        {
            var result = "";

            foreach (var element in QualityBoard)
            {
                result = result + element.Key + " : " + element.Value + "\n";
            }

            return result;
        }
    }
}
