﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToeAI
{
    public class Board
    {
        public int ColumnRow = 3;

        public string[] BoardValue { get; }

        public Board()
        {
            BoardValue = new string[ColumnRow * ColumnRow];
            InitBoard();
        }

        public void InitBoard()
        {
            for (int i = 0; i < ColumnRow * ColumnRow; i++)
            {
                BoardValue[i] = string.Empty;
            }
        }

        public void Add(string pawn, int location)
        {
            BoardValue[location] = pawn;
        }

        public IEnumerable<int> GetPossibleCase()
        {
            for (int i = 0; i < ColumnRow * ColumnRow; i++)
            {
                if (string.IsNullOrEmpty(BoardValue[i]))
                    yield return i;
            }
        }

        public bool IsComplete()
        {
            var isEmpty = false;
            for(var i = 0; i < ColumnRow * ColumnRow; i++)
            {
                isEmpty = isEmpty || string.IsNullOrEmpty(BoardValue[i]);
            }
            return !isEmpty;
        }

        public int GetPawnNumber(string pawn)
        {
            var count = 0;
            for (var i = 0; i < ColumnRow * ColumnRow; i++)
            {
                if (BoardValue[i] == pawn)
                    count++;
            }
            return count;
        }

        public bool TryGetWinner(string currentPawn)
        {
            //horizontal
            for (var k = 0; k < ColumnRow; k++)
            {
                var consecutive = true;
                for (var i = (k*3); i < (k+1)*3; i++)
                {
                    consecutive = consecutive && (BoardValue[i] == currentPawn);
                }
                if (consecutive)
                    return true;
            }

            //vertical
            for (var k = 0; k < ColumnRow; k++)
            {
                var consecutive = true;
                for (var i = k; i < ((ColumnRow * (ColumnRow -1)) + k + 1); i = i+ ColumnRow)
                {
                    consecutive = consecutive && (BoardValue[i] == currentPawn);
                }
                if (consecutive)
                    return true;
            }

            //diagonal
            if (BoardValue[0] == BoardValue[4] && BoardValue[4] == BoardValue[8] && BoardValue[8] == currentPawn)
                return true;

            if (BoardValue[6] == BoardValue[4] && BoardValue[4] == BoardValue[2] && BoardValue[2] == currentPawn)
                return true;

            return false;
        }

        public override string ToString()
        {
            var result = "";
            for (var i = 0; i < ColumnRow; i++)
            {
                for (var j = 0; j < ColumnRow; j++)
                {
                    var index = j + (i * 3);
                    var value = string.IsNullOrEmpty(BoardValue[index]) ? " " : BoardValue[index];
                    result = result + "[" + value + "]";
                }
                result = result + "\n";
            }

            return result;
        }

        public string ToHash()
        {
            var result = "";
            for(var i = 0; i < ColumnRow * ColumnRow; i++)
            {
                if (!string.IsNullOrEmpty(BoardValue[i]))
                {
                    result = result + $"{i}" + BoardValue[i];
                }
            }

            if(string.IsNullOrEmpty(result))
            {
                return "EmptyBoard";
            }

            return result;
        }

        public string ToHash(int location, string value)
        {
            var result = "";
            for (var i = 0; i < ColumnRow * ColumnRow; i++)
            {
                if (!string.IsNullOrEmpty(BoardValue[i]))
                {
                    result = result + $"{i}" + BoardValue[i];
                }

                if(string.IsNullOrEmpty(BoardValue[i]) && i == location)
                {
                    result = result + $"{i}" + value;
                }
            }

            return result;
        }
    }
}
