﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeAI
{
    class Program
    {
        static void Main(string[] args)
        {
            var playBoard = new Board();
            Console.WriteLine("Iterations number");
            var epochs = int.Parse(Console.ReadLine());
            var count = 0;

            var player1 = new PlayerAI("x");
            Console.WriteLine("Player 1 pawn : '" + player1.Pawn + "' ");
            var player2 = new PlayerAI("o");
            Console.WriteLine("Player AI pawn : '" + player2.Pawn + "' ");

            while (count <= epochs)
            {
                playBoard.InitBoard();

                Console.WriteLine("--------- Current Board ---------");
                Console.Write(playBoard);

                while (!playBoard.TryGetWinner(player1.Pawn) || !playBoard.TryGetWinner(player2.Pawn) || playBoard.IsComplete())
                {
                 
                    playBoard.Add(player1.Pawn, player1.GetRandomNextCase(playBoard));
                    player2.AIPlay(playBoard, player1, player2.Pawn, 0.5, 0.5);

                    Console.WriteLine("\n--------- Player 1 Play ---------");
                    Console.Write(playBoard);

                    if (playBoard.TryGetWinner("x") || playBoard.IsComplete())
                        break;

                    playBoard.Add(player2.Pawn, player2.GetRandomNextCase(playBoard));
                    player2.AIPlay(playBoard, player1, player1.Pawn, 0.5, 0.5);

                    Console.WriteLine("\n--------- AI Play ---------");
                    Console.Write(playBoard);

                    if (playBoard.TryGetWinner("o") || playBoard.IsComplete())
                        break;
                }

                Console.WriteLine("\n--------- End ---------");
                Console.Write(playBoard);
                count++;
            }

            Console.WriteLine("\n--------- Training end ---------");

            while (true)
            {
                playBoard.InitBoard();

                Console.WriteLine("--------- Current Board ---------");
                Console.Write(playBoard);

                while (!playBoard.TryGetWinner(player1.Pawn) || !playBoard.TryGetWinner(player2.Pawn) || playBoard.IsComplete())
                {
                    int play;
                    string playerPlay = " ";
                    while (!int.TryParse(playerPlay, out play) || !string.IsNullOrEmpty(playBoard.BoardValue[play]))
                    {
                        playerPlay = Console.ReadLine();
                        Console.WriteLine("Player 1, your play (0 - 8)");
                        if (playerPlay == "q1")
                        {
                            Console.WriteLine(player1.ToQualityString());
                            playerPlay = " ";
                        }
                        if (playerPlay == "q2")
                        {
                            Console.WriteLine(player2.ToQualityString());
                            playerPlay = " ";
                        }
                    }


                    playBoard.Add("x", play);
                    player2.AIPlay(playBoard, player1, player2.Pawn, 0.5, 0.5);

                    Console.WriteLine("\n--------- Player 1 Play ---------");
                    Console.Write(playBoard);

                    if (playBoard.TryGetWinner("x") || playBoard.IsComplete())
                        break;

                    double? bestQ = null;
                    var location = -1;
                    var possibilities = playBoard.GetPossibleCase().ToArray();
                    for (var i = 0; i < possibilities.Count(); i++)
                    {
                        var currentQ = player2.QualityBoard.TryGetValue(playBoard.ToHash(possibilities[i], player2.Pawn), out var value) ? value : 0;
                        if (bestQ == null || currentQ >= bestQ)
                        {
                            bestQ = currentQ;
                            location = possibilities[i];
                        }
                    }

                    playBoard.Add(player2.Pawn, location);
                    player2.AIPlay(playBoard, player1, player1.Pawn, 0.8, 0.7);

                    Console.WriteLine("\n--------- AI Play ---------");
                    Console.Write(playBoard);

                    if (playBoard.TryGetWinner("o") || playBoard.IsComplete())
                        break;
                }
            }
        }
    }
}
